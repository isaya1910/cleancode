package nitro.kz.cleanarchitecture.di

import android.arch.persistence.room.Room
import android.content.Context
import nitro.kz.cleanarchitecture.api.ImagesApi
import nitro.kz.cleanarchitecture.database.DaoService
import nitro.kz.cleanarchitecture.database.DataBase
import nitro.kz.cleanarchitecture.interactors.InteractorImpl
import nitro.kz.cleanarchitecture.presenters.ImagesPresenter
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val appModule = module {
    single {
        createDataBaseRoom(androidContext())
    }
    factory {
        createOkHttpClient()
    }
    single {
        createWebService<ImagesApi>(get(), baseUrl)
    }
    module("interactor") {
        factory {
            InteractorImpl(get(), get())
        }
        module("presenter"){
            factory {
                ImagesPresenter(get())
            }
        }
    }
}


fun createOkHttpClient(): OkHttpClient {
    val httpLoggingInterceptor = HttpLoggingInterceptor()
    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
    var okHttpBuilder = OkHttpClient.Builder()
            .connectTimeout(60L, TimeUnit.SECONDS)
            .readTimeout(60L, TimeUnit.SECONDS)
            .addInterceptor(httpLoggingInterceptor)
    return okHttpBuilder.build()

}
private  val baseUrl = "https://83685485-b7c2-4fb4-9af9-6e03d6067eb0.mock.pstmn.io/"
inline fun <reified T> createWebService(okHttpClient: OkHttpClient, url: String): T {

    val retrofit = Retrofit.Builder()
            .baseUrl(url)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build()
    return retrofit.create(T::class.java)
}

fun createDataBaseRoom(context: Context): DaoService {

    return Room.databaseBuilder(context, DataBase::class.java, "clean.db")
            .build().projectDao()
}