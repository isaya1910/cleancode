package nitro.kz.cleanarchitecture.api

import io.reactivex.Single
import nitro.kz.cleanarchitecture.entities.ImagesResponse
import retrofit2.Response
import retrofit2.http.GET

interface ImagesApi {
    @GET("images")
    fun getImages(): Single<Response<ImagesResponse>>
}