package nitro.kz.cleanarchitecture.views

import nitro.kz.cleanarchitecture.entities.Image

interface ImagesView: BaseView {
    fun showImages(items: ArrayList<Image>)
    fun showSuggestions(items: ArrayList<String>)
}