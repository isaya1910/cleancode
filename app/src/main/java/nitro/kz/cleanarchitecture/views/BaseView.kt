package nitro.kz.cleanarchitecture.views

interface BaseView {
    fun startLoad()
    fun stopLoad()
    fun showMessage(text: String)
}