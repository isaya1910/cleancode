package nitro.kz.cleanarchitecture.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import android.arch.persistence.room.Update
import io.reactivex.Single
import nitro.kz.cleanarchitecture.entities.Suggestion

@Dao
interface DaoService {
    @Insert(onConflict = REPLACE)
    fun insertSuggestion(suggestion: Suggestion)

    @Update(onConflict = REPLACE)
    fun updateSuggestion(suggestion: Suggestion)

    @Query("SELECT * From suggestion")
    fun getSuggestions(): Single<List<Suggestion>>
}