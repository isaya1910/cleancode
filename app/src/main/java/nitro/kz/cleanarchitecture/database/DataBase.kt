package nitro.kz.cleanarchitecture.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import nitro.kz.cleanarchitecture.entities.Suggestion


@Database(entities = [
   Suggestion:: class
], version = 1, exportSchema = false)
abstract class DataBase : RoomDatabase() {
    abstract fun projectDao(): DaoService
}