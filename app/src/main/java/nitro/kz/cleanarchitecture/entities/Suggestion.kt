package nitro.kz.cleanarchitecture.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

import io.realm.RealmObject

@Entity(tableName = "suggestion")
open class Suggestion {
    @PrimaryKey
    var data = ""
}
