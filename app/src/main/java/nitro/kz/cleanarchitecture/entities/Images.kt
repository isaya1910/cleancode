package nitro.kz.cleanarchitecture.entities

data class Image(var title: String, var url: String)

data class ImagesResponse(var images:ArrayList<Image>)
