package nitro.kz.cleanarchitecture.images

import android.widget.ImageView

interface OnImageClickedListener {
    fun onClicked(url: String,imageView: ImageView)
}