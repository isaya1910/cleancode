package nitro.kz.cleanarchitecture.images

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_image_detail.*
import nitro.kz.cleanarchitecture.R

class ImageDetailActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_detail)
        Glide.with(this).load(intent.getStringExtra("url")).into(imageDetailView)
    }
}