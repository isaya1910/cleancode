package nitro.kz.cleanarchitecture.images

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.SearchView
import android.view.Menu
import android.widget.*
import nitro.kz.cleanarchitecture.R
import nitro.kz.cleanarchitecture.presenters.ImagesPresenter
import nitro.kz.cleanarchitecture.views.ImagesView
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.view.ViewCompat
import kotlinx.android.synthetic.main.activity_images.*
import nitro.kz.cleanarchitecture.entities.Image
import org.koin.android.ext.android.inject


class ImagesActivity : AppCompatActivity(),ImagesView,OnImageClickedListener {
    override fun showImages(items: ArrayList<Image>) {
        imagesAdapter?.updateList(items)
    }

    override fun onClicked(url: String,imageView: ImageView) {
        //open image detail activity with shared element transition
        val intent = Intent(this,ImageDetailActivity::class.java)
        intent.putExtra("url",url)
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this,
                imageView,
                ViewCompat.getTransitionName(imageView))
        startActivity(intent, options.toBundle())
    }

    override fun showMessage(text: String) {
        Toast.makeText(this,text,Toast.LENGTH_LONG).show()
    }


    override fun onDestroy() {
        super.onDestroy()
        presenter.stop()
    }

    override fun startLoad() {
        swipeRefresh.isRefreshing = true
    }

    override fun stopLoad() {
        swipeRefresh.isRefreshing = false
    }

    override fun showSuggestions(items: ArrayList<String>){
        suggestionAdapter = ArrayAdapter(this,
                android.R.layout.simple_dropdown_item_1line, items.toTypedArray())

        autoCompleteTextView?.setAdapter(suggestionAdapter)
    }
    private var suggestionAdapter : ArrayAdapter<String>?=null

    private var searchView: SearchView?=null
    private var autoCompleteTextView:AutoCompleteTextView?=null
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_menu,menu)
        val searchItem = menu?.findItem(R.id.actionSearch)
        searchView = searchItem?.actionView as SearchView
        searchView?.queryHint = "Search"
        autoCompleteTextView = searchView?.findViewById(android.support.v7.appcompat.R.id.search_src_text) as AutoCompleteTextView
        autoCompleteTextView?.setAdapter(suggestionAdapter)
        autoCompleteTextView?.threshold = 0
        autoCompleteTextView?.onItemClickListener = AdapterView.OnItemClickListener {
            _, _, position, _ ->
            imagesAdapter?.filter(presenter.suggestionLabels[position])
        }
        presenter.getSuggestions()
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                query.let {
                    if (query!!.isNotEmpty()) {
                        presenter.onSearchClicked(query)
                        presenter.getSuggestions()
                    }
                }
                return true

            }

            override fun onQueryTextChange(newText: String?): Boolean {
                newText?.let {
                   // filter by car model title
                    imagesAdapter?.filter(newText)
                }
                return true
            }
        })
        return super.onCreateOptionsMenu(menu)
    }

    var imagesAdapter: ImagesAdapter?=null
    private val presenter: ImagesPresenter by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_images)
        presenter.attachView(this)
        swipeRefresh.setOnRefreshListener {
            presenter.showImages()
        }
        imagesAdapter = ImagesAdapter(this)
        recyclerView.layoutManager = GridLayoutManager(this,3)
        recyclerView.adapter = imagesAdapter
        suggestionAdapter = ArrayAdapter(this,
                android.R.layout.simple_dropdown_item_1line, presenter.suggestionLabels.toTypedArray())
        presenter.getSuggestions()
        presenter.showImages()

    }
}
