package nitro.kz.cleanarchitecture.images

import nitro.kz.cleanarchitecture.entities.Image

class ImagesQueryFilter(var items: ArrayList<Image>?){
    fun findByFilter(query: String): ArrayList<Image>{
        var ans = ArrayList<Image>()
        items?.forEach {
            if (it.title.toLowerCase().contains(query.toLowerCase())){
                ans.add(it)
            }
        }
        return ans
    }
}