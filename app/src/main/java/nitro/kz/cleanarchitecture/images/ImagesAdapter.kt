package nitro.kz.cleanarchitecture.images

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import nitro.kz.cleanarchitecture.R
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import kotlinx.android.synthetic.main.item_image.view.*
import nitro.kz.cleanarchitecture.entities.Image


class ImagesAdapter(val onImageClickedListener: OnImageClickedListener): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var imagesQuery: ImagesQueryFilter?=null
    var items: ArrayList<Image>?=null

    fun updateList(items: ArrayList<Image>){
        this.items = items
        imagesQuery = ImagesQueryFilter(this.items)
        notifyDataSetChanged()
    }

    fun filter(query: String){
        items = imagesQuery?.findByFilter(query)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ImageViewHolder(LayoutInflater.from(parent.context).inflate(
                R.layout.item_image,parent,false))
    }

    private fun setScaleAnimation(view: View) {
        //animation to on bind
        val anim = ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
        anim.duration = 200
        view.startAnimation(anim)
    }



    override fun getItemCount(): Int {
        items?.let {
            return items?.size!!
        }
        return 0
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ImageViewHolder){
            Glide.with(holder.itemView.context).load(items?.get(position)?.url).into(holder.itemView.imageView)
        }
    }

    inner class ImageViewHolder (view: View): RecyclerView.ViewHolder(view),View.OnClickListener{
        init {
            view.setOnClickListener(this)
        }
        override fun onClick(v: View?) {
            onImageClickedListener.onClicked(items?.get(adapterPosition)?.url!!, this.itemView.imageView)
        }
    }



}