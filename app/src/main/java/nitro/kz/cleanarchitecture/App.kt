package nitro.kz.cleanarchitecture

import android.app.Application
import io.realm.Realm
import android.arch.persistence.room.Room
import nitro.kz.cleanarchitecture.di.appModule
import nitro.kz.cleanarchitecture.di.start
import org.koin.android.ext.koin.with
import org.koin.standalone.StandAloneContext.startKoin


class App: Application() {

    override fun onCreate() {
        super.onCreate()
        start(this)
    }

}