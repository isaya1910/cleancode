package nitro.kz.cleanarchitecture.presenters

import io.reactivex.android.schedulers.AndroidSchedulers
import nitro.kz.cleanarchitecture.entities.Image
import nitro.kz.cleanarchitecture.interactors.InteractorImpl
import nitro.kz.cleanarchitecture.views.ImagesView

class ImagesPresenter(private val interactor: InteractorImpl): BasePresenter<ImagesView>(){
    var images = ArrayList<Image>()
    var suggestionLabels = ArrayList<String>()
    fun showImages(){
        launch {
            interactor.getImagesUseCase()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { view?.startLoad() }
                .doFinally { view?.stopLoad() }
                .subscribe(
                        {
                            if (it.isSuccessful){
                                it.body()?.images?.let {
                                    images.clear()
                                    images.addAll(images)
                                    view?.showImages(it)
                                }
                            }
                        },
                        {
                            it.printStackTrace()
                        }
                )
        }
    }
    fun onSearchClicked(query: String){
        interactor.submitQueryUseCase(query)
    }

    fun getSuggestions(){
        launch {
            interactor.getSuggestionsListUseCase()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe (
                            {
                                suggestionLabels.clear()
                                it.forEach {
                                    suggestionLabels.add(it.data)
                                }
                                view?.showSuggestions(suggestionLabels)
                            },{
                                it.printStackTrace()
                            }
                    )
        }
    }
}