package nitro.kz.cleanarchitecture.presenters

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable


abstract class BasePresenter <V>{
    var view: V?=null
    fun attachView(view: V){
        this.view = view
    }
    private fun detachView(){
        view = null
    }
    private var compositeDisposable = CompositeDisposable()
    fun stop(){
        compositeDisposable.clear()
        detachView()
    }
    fun launch(job: () -> Disposable) {
        compositeDisposable.add(job())
    }
}