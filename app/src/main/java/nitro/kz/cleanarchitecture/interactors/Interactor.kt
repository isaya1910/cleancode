package nitro.kz.cleanarchitecture.interactors

import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.experimental.GlobalScope
import kotlinx.coroutines.experimental.launch
import nitro.kz.cleanarchitecture.api.ImagesApi
import nitro.kz.cleanarchitecture.database.DaoService
import nitro.kz.cleanarchitecture.entities.ImagesResponse
import nitro.kz.cleanarchitecture.entities.Suggestion
import retrofit2.Response


class InteractorImpl(private val imagesApi: ImagesApi,private val daoService: DaoService): Interactor{
    override fun getSuggestionsListUseCase(): Single<List<Suggestion>> {
        return daoService.getSuggestions().subscribeOn(Schedulers.io())
    }

    override fun getImagesUseCase(): Single<Response<ImagesResponse>> {
        return imagesApi.getImages().subscribeOn(Schedulers.io())
    }

    override fun submitQueryUseCase(query: String) {
        GlobalScope.launch {
            var suggestion = Suggestion()
            suggestion.data = query
            daoService.insertSuggestion(suggestion)
        }

    }
}
interface Interactor{
    fun getImagesUseCase(): Single<Response<ImagesResponse>>
    fun submitQueryUseCase(query: String)
    fun getSuggestionsListUseCase(): Single<List<Suggestion>>
}
