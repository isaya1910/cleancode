package nitro.kz.cleanarchitecture.presenters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b&\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0013\u0010\f\u001a\u00020\r2\u0006\u0010\u0006\u001a\u00028\u0000\u00a2\u0006\u0002\u0010\nJ\b\u0010\u000e\u001a\u00020\rH\u0002J\u0014\u0010\u000f\u001a\u00020\r2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011J\u0006\u0010\u0013\u001a\u00020\rR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0006\u001a\u0004\u0018\u00018\u0000X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u000b\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\n\u00a8\u0006\u0014"}, d2 = {"Lnitro/kz/cleanarchitecture/presenters/BasePresenter;", "V", "", "()V", "compositeDisposable", "Lio/reactivex/disposables/CompositeDisposable;", "view", "getView", "()Ljava/lang/Object;", "setView", "(Ljava/lang/Object;)V", "Ljava/lang/Object;", "attachView", "", "detachView", "launch", "job", "Lkotlin/Function0;", "Lio/reactivex/disposables/Disposable;", "stop", "app_debug"})
public abstract class BasePresenter<V extends java.lang.Object> {
    @org.jetbrains.annotations.Nullable()
    private V view;
    private io.reactivex.disposables.CompositeDisposable compositeDisposable;
    
    @org.jetbrains.annotations.Nullable()
    public final V getView() {
        return null;
    }
    
    public final void setView(@org.jetbrains.annotations.Nullable()
    V p0) {
    }
    
    public final void attachView(V view) {
    }
    
    private final void detachView() {
    }
    
    public final void stop() {
    }
    
    public final void launch(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function0<? extends io.reactivex.disposables.Disposable> job) {
    }
    
    public BasePresenter() {
        super();
    }
}