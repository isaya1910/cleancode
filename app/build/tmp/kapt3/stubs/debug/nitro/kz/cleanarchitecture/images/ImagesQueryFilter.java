package nitro.kz.cleanarchitecture.images;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B!\u0012\u001a\u0010\u0002\u001a\u0016\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003j\n\u0012\u0004\u0012\u00020\u0004\u0018\u0001`\u0005\u00a2\u0006\u0002\u0010\u0006J\u001e\u0010\n\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u00052\u0006\u0010\u000b\u001a\u00020\fR.\u0010\u0002\u001a\u0016\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003j\n\u0012\u0004\u0012\u00020\u0004\u0018\u0001`\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\u0006\u00a8\u0006\r"}, d2 = {"Lnitro/kz/cleanarchitecture/images/ImagesQueryFilter;", "", "items", "Ljava/util/ArrayList;", "Lnitro/kz/cleanarchitecture/entities/Image;", "Lkotlin/collections/ArrayList;", "(Ljava/util/ArrayList;)V", "getItems", "()Ljava/util/ArrayList;", "setItems", "findByFilter", "query", "", "app_debug"})
public final class ImagesQueryFilter {
    @org.jetbrains.annotations.Nullable()
    private java.util.ArrayList<nitro.kz.cleanarchitecture.entities.Image> items;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<nitro.kz.cleanarchitecture.entities.Image> findByFilter(@org.jetbrains.annotations.NotNull()
    java.lang.String query) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.ArrayList<nitro.kz.cleanarchitecture.entities.Image> getItems() {
        return null;
    }
    
    public final void setItems(@org.jetbrains.annotations.Nullable()
    java.util.ArrayList<nitro.kz.cleanarchitecture.entities.Image> p0) {
    }
    
    public ImagesQueryFilter(@org.jetbrains.annotations.Nullable()
    java.util.ArrayList<nitro.kz.cleanarchitecture.entities.Image> items) {
        super();
    }
}