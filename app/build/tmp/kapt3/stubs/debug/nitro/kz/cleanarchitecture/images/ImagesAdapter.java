package nitro.kz.cleanarchitecture.images;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\'B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u000e\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019J\b\u0010\u001a\u001a\u00020\u001bH\u0016J\u0018\u0010\u001c\u001a\u00020\u00172\u0006\u0010\u001d\u001a\u00020\u00022\u0006\u0010\u001e\u001a\u00020\u001bH\u0016J\u0018\u0010\u001f\u001a\u00020\u00022\u0006\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020\u001bH\u0016J\u0010\u0010#\u001a\u00020\u00172\u0006\u0010$\u001a\u00020%H\u0002J\u001e\u0010&\u001a\u00020\u00172\u0016\u0010\f\u001a\u0012\u0012\u0004\u0012\u00020\u000e0\rj\b\u0012\u0004\u0012\u00020\u000e`\u000fR\u001c\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR.\u0010\f\u001a\u0016\u0012\u0004\u0012\u00020\u000e\u0018\u00010\rj\n\u0012\u0004\u0012\u00020\u000e\u0018\u0001`\u000fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015\u00a8\u0006("}, d2 = {"Lnitro/kz/cleanarchitecture/images/ImagesAdapter;", "Landroid/support/v7/widget/RecyclerView$Adapter;", "Landroid/support/v7/widget/RecyclerView$ViewHolder;", "onImageClickedListener", "Lnitro/kz/cleanarchitecture/images/OnImageClickedListener;", "(Lnitro/kz/cleanarchitecture/images/OnImageClickedListener;)V", "imagesQuery", "Lnitro/kz/cleanarchitecture/images/ImagesQueryFilter;", "getImagesQuery", "()Lnitro/kz/cleanarchitecture/images/ImagesQueryFilter;", "setImagesQuery", "(Lnitro/kz/cleanarchitecture/images/ImagesQueryFilter;)V", "items", "Ljava/util/ArrayList;", "Lnitro/kz/cleanarchitecture/entities/Image;", "Lkotlin/collections/ArrayList;", "getItems", "()Ljava/util/ArrayList;", "setItems", "(Ljava/util/ArrayList;)V", "getOnImageClickedListener", "()Lnitro/kz/cleanarchitecture/images/OnImageClickedListener;", "filter", "", "query", "", "getItemCount", "", "onBindViewHolder", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "setScaleAnimation", "view", "Landroid/view/View;", "updateList", "ImageViewHolder", "app_debug"})
public final class ImagesAdapter extends android.support.v7.widget.RecyclerView.Adapter<android.support.v7.widget.RecyclerView.ViewHolder> {
    @org.jetbrains.annotations.Nullable()
    private nitro.kz.cleanarchitecture.images.ImagesQueryFilter imagesQuery;
    @org.jetbrains.annotations.Nullable()
    private java.util.ArrayList<nitro.kz.cleanarchitecture.entities.Image> items;
    @org.jetbrains.annotations.NotNull()
    private final nitro.kz.cleanarchitecture.images.OnImageClickedListener onImageClickedListener = null;
    
    @org.jetbrains.annotations.Nullable()
    public final nitro.kz.cleanarchitecture.images.ImagesQueryFilter getImagesQuery() {
        return null;
    }
    
    public final void setImagesQuery(@org.jetbrains.annotations.Nullable()
    nitro.kz.cleanarchitecture.images.ImagesQueryFilter p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.ArrayList<nitro.kz.cleanarchitecture.entities.Image> getItems() {
        return null;
    }
    
    public final void setItems(@org.jetbrains.annotations.Nullable()
    java.util.ArrayList<nitro.kz.cleanarchitecture.entities.Image> p0) {
    }
    
    public final void updateList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<nitro.kz.cleanarchitecture.entities.Image> items) {
    }
    
    public final void filter(@org.jetbrains.annotations.NotNull()
    java.lang.String query) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.support.v7.widget.RecyclerView.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    private final void setScaleAnimation(android.view.View view) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    android.support.v7.widget.RecyclerView.ViewHolder holder, int position) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final nitro.kz.cleanarchitecture.images.OnImageClickedListener getOnImageClickedListener() {
        return null;
    }
    
    public ImagesAdapter(@org.jetbrains.annotations.NotNull()
    nitro.kz.cleanarchitecture.images.OnImageClickedListener onImageClickedListener) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0086\u0004\u0018\u00002\u00020\u00012\u00020\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0012\u0010\u0006\u001a\u00020\u00072\b\u0010\b\u001a\u0004\u0018\u00010\u0004H\u0016\u00a8\u0006\t"}, d2 = {"Lnitro/kz/cleanarchitecture/images/ImagesAdapter$ImageViewHolder;", "Landroid/support/v7/widget/RecyclerView$ViewHolder;", "Landroid/view/View$OnClickListener;", "view", "Landroid/view/View;", "(Lnitro/kz/cleanarchitecture/images/ImagesAdapter;Landroid/view/View;)V", "onClick", "", "v", "app_debug"})
    public final class ImageViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder implements android.view.View.OnClickListener {
        
        @java.lang.Override()
        public void onClick(@org.jetbrains.annotations.Nullable()
        android.view.View v) {
        }
        
        public ImageViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View view) {
            super(null);
        }
    }
}