package nitro.kz.cleanarchitecture.images;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u0000n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u00172\u0006\u0010\u001b\u001a\u00020\u001cH\u0016J\u0012\u0010\u001d\u001a\u00020\u00192\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0014J\u0012\u0010 \u001a\u00020!2\b\u0010\"\u001a\u0004\u0018\u00010#H\u0016J\b\u0010$\u001a\u00020\u0019H\u0014J \u0010%\u001a\u00020\u00192\u0016\u0010&\u001a\u0012\u0012\u0004\u0012\u00020(0\'j\b\u0012\u0004\u0012\u00020(`)H\u0016J\u0010\u0010*\u001a\u00020\u00192\u0006\u0010+\u001a\u00020\u0017H\u0016J \u0010,\u001a\u00020\u00192\u0016\u0010&\u001a\u0012\u0012\u0004\u0012\u00020\u00170\'j\b\u0012\u0004\u0012\u00020\u0017`)H\u0016J\b\u0010-\u001a\u00020\u0019H\u0016J\b\u0010.\u001a\u00020\u0019H\u0016R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u001b\u0010\r\u001a\u00020\u000e8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u000f\u0010\u0010R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0015\u001a\n\u0012\u0004\u0012\u00020\u0017\u0018\u00010\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006/"}, d2 = {"Lnitro/kz/cleanarchitecture/images/ImagesActivity;", "Landroid/support/v7/app/AppCompatActivity;", "Lnitro/kz/cleanarchitecture/views/ImagesView;", "Lnitro/kz/cleanarchitecture/images/OnImageClickedListener;", "()V", "autoCompleteTextView", "Landroid/widget/AutoCompleteTextView;", "imagesAdapter", "Lnitro/kz/cleanarchitecture/images/ImagesAdapter;", "getImagesAdapter", "()Lnitro/kz/cleanarchitecture/images/ImagesAdapter;", "setImagesAdapter", "(Lnitro/kz/cleanarchitecture/images/ImagesAdapter;)V", "presenter", "Lnitro/kz/cleanarchitecture/presenters/ImagesPresenter;", "getPresenter", "()Lnitro/kz/cleanarchitecture/presenters/ImagesPresenter;", "presenter$delegate", "Lkotlin/Lazy;", "searchView", "Landroid/support/v7/widget/SearchView;", "suggestionAdapter", "Landroid/widget/ArrayAdapter;", "", "onClicked", "", "url", "imageView", "Landroid/widget/ImageView;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateOptionsMenu", "", "menu", "Landroid/view/Menu;", "onDestroy", "showImages", "items", "Ljava/util/ArrayList;", "Lnitro/kz/cleanarchitecture/entities/Image;", "Lkotlin/collections/ArrayList;", "showMessage", "text", "showSuggestions", "startLoad", "stopLoad", "app_debug"})
public final class ImagesActivity extends android.support.v7.app.AppCompatActivity implements nitro.kz.cleanarchitecture.views.ImagesView, nitro.kz.cleanarchitecture.images.OnImageClickedListener {
    private android.widget.ArrayAdapter<java.lang.String> suggestionAdapter;
    private android.support.v7.widget.SearchView searchView;
    private android.widget.AutoCompleteTextView autoCompleteTextView;
    @org.jetbrains.annotations.Nullable()
    private nitro.kz.cleanarchitecture.images.ImagesAdapter imagesAdapter;
    private final kotlin.Lazy presenter$delegate = null;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    public void showImages(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<nitro.kz.cleanarchitecture.entities.Image> items) {
    }
    
    @java.lang.Override()
    public void onClicked(@org.jetbrains.annotations.NotNull()
    java.lang.String url, @org.jetbrains.annotations.NotNull()
    android.widget.ImageView imageView) {
    }
    
    @java.lang.Override()
    public void showMessage(@org.jetbrains.annotations.NotNull()
    java.lang.String text) {
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    @java.lang.Override()
    public void startLoad() {
    }
    
    @java.lang.Override()
    public void stopLoad() {
    }
    
    @java.lang.Override()
    public void showSuggestions(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<java.lang.String> items) {
    }
    
    @java.lang.Override()
    public boolean onCreateOptionsMenu(@org.jetbrains.annotations.Nullable()
    android.view.Menu menu) {
        return false;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final nitro.kz.cleanarchitecture.images.ImagesAdapter getImagesAdapter() {
        return null;
    }
    
    public final void setImagesAdapter(@org.jetbrains.annotations.Nullable()
    nitro.kz.cleanarchitecture.images.ImagesAdapter p0) {
    }
    
    private final nitro.kz.cleanarchitecture.presenters.ImagesPresenter getPresenter() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public ImagesActivity() {
        super();
    }
}