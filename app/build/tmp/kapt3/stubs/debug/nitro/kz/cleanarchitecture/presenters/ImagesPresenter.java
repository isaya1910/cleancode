package nitro.kz.cleanarchitecture.presenters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 10}, bv = {1, 0, 2}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0006\u0010\u0012\u001a\u00020\u0013J\u000e\u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0015\u001a\u00020\u000fJ\u0006\u0010\u0016\u001a\u00020\u0013R*\u0010\u0006\u001a\u0012\u0012\u0004\u0012\u00020\b0\u0007j\b\u0012\u0004\u0012\u00020\b`\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R*\u0010\u000e\u001a\u0012\u0012\u0004\u0012\u00020\u000f0\u0007j\b\u0012\u0004\u0012\u00020\u000f`\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u000b\"\u0004\b\u0011\u0010\r\u00a8\u0006\u0017"}, d2 = {"Lnitro/kz/cleanarchitecture/presenters/ImagesPresenter;", "Lnitro/kz/cleanarchitecture/presenters/BasePresenter;", "Lnitro/kz/cleanarchitecture/views/ImagesView;", "interactor", "Lnitro/kz/cleanarchitecture/interactors/InteractorImpl;", "(Lnitro/kz/cleanarchitecture/interactors/InteractorImpl;)V", "images", "Ljava/util/ArrayList;", "Lnitro/kz/cleanarchitecture/entities/Image;", "Lkotlin/collections/ArrayList;", "getImages", "()Ljava/util/ArrayList;", "setImages", "(Ljava/util/ArrayList;)V", "suggestionLabels", "", "getSuggestionLabels", "setSuggestionLabels", "getSuggestions", "", "onSearchClicked", "query", "showImages", "app_debug"})
public final class ImagesPresenter extends nitro.kz.cleanarchitecture.presenters.BasePresenter<nitro.kz.cleanarchitecture.views.ImagesView> {
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<nitro.kz.cleanarchitecture.entities.Image> images;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<java.lang.String> suggestionLabels;
    private final nitro.kz.cleanarchitecture.interactors.InteractorImpl interactor = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<nitro.kz.cleanarchitecture.entities.Image> getImages() {
        return null;
    }
    
    public final void setImages(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<nitro.kz.cleanarchitecture.entities.Image> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<java.lang.String> getSuggestionLabels() {
        return null;
    }
    
    public final void setSuggestionLabels(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<java.lang.String> p0) {
    }
    
    public final void showImages() {
    }
    
    public final void onSearchClicked(@org.jetbrains.annotations.NotNull()
    java.lang.String query) {
    }
    
    public final void getSuggestions() {
    }
    
    public ImagesPresenter(@org.jetbrains.annotations.NotNull()
    nitro.kz.cleanarchitecture.interactors.InteractorImpl interactor) {
        super();
    }
}